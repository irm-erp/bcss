<ol class="breadcrumbs">
    <li> <a href="{{ url("/") }}"> <i class="fa fa-home"></i> </a> </li>

    @foreach ($path as $menu)
	    <li> 
	    	<a href="{{ $menu->url!==null ? url($menu->url) : "#" }}"> 
	    		<span>{{ trans_label($menu->title) }}</span>
	    	</a> 
	    </li>
    @endforeach
</ol>