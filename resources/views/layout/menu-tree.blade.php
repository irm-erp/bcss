<?php

// Class
$class = isset($attr["class"]) ? [$attr["class"]] : [];

if ($active) 
    $class[] = "active";

// Attributes
$attribute = "";
unset($attr["class"]);

foreach ($attr as $name => $value) {
    $attribute .= " $name='$value'";
}

?>

<li {!! count($class) ? "class='".implode(" ", $class)."'" : "" !!} {!! $attribute !!}>
    <a @if (isset($dropdown)) class="menu-dropdown" @endif
        href='{{ !empty($url) ? url($url) : "#" }}'>
        @if (!empty($icon)) <i class='menu-icon {{ $icon }}'></i> @endif
        <span class="menu-text">@lang($label)</span>
        @if (isset($dropdown)) <i class="fa fa-chevron-right menu-expand"></i> @endif
    </a>

    @if (isset($dropdown))
        <ul class='submenu'>{!! $dropdown !!}</ul>
    @endif
</li>