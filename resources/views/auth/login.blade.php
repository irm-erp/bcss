@extends("auth.app")

@section("title", "Login")

@section("content-auth")
  {!! Form::open(["url" => "login"]) !!}
    <div class="loginbox-textbox">
      <input type="text" name="email" class="form-control" placeholder="Email" 
        value="{{ old('email') }}" />
    </div>
    <div class="loginbox-textbox">
      <input name="password" type="password" class="form-control" />
    </div>

    <div class="loginbox-forgot">
      <div class="checkbox-custom checkbox-default">
        <input id="RememberMe" name="remember" type="checkbox" />
        <label for="RememberMe">Remember Me</label>
      </div>
    </div>

    <div class="loginbox-submit">
      <input type="submit" class="btn btn-primary btn-block" value="Login">
    </div>

    <div class="text-center">
      <a href="{{ url('/password/reset') }}" class="text-sm text-muted">Forgot Password?</a>
    </div>
  {!! Form::close() !!}
@endsection