<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Login</title>

    <meta name="description" content="login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="shortcut icon" href="{{ asset("img/favicon.ico") }}" />
    <link rel="stylesheet" href="{{ asset("css/app.css") }}" />
    @stack("style")
</head>

<body>
  {{-- Flash Messages --}}
  @if (Flash::exists())
      @foreach (Flash::pull() as $state => $messages)
          @foreach ($messages as $item)
              <div class="alert alert-{{ $state }}">
                  <button type="button" class="close" data-dismiss="alert">
                      <span>&times;</span>
                  </button>

                  {!! $item !!}
              </div>
          @endforeach
      @endforeach
  @endif

  {{-- Errors --}}
  @if (isset($errors) && count($errors) > 0)
      <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert">
              <span>&times;</span>
          </button>
          
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

  <div class="login-container animated fadeInDown">
    <div class="loginbox bg-white">
      <div class="loginbox-title">SIGN IN</div><hr>
    	@yield("content-auth")
    </div>
  </div>

  <script src="{{ asset("js/app.js") }}"> </script>
  @stack("script")
</body>
</html>