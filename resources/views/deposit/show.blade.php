@extends("app")

@section("content")
  <style type="text/css">
    img.thumbnail {
      max-height: 200px;
      max-width: 200px;
    }
  </style>

  {!! Form::model($model, ["url" => "deposit/$model->id"]) !!}
    {{ method_field("PUT") }}

    <div class="panel panel-default">
      <div class="panel-body">
        <div class="form-group col-md-6">
          <label class="control-label text-right col-md-6">
            <b>{{ $model->label("tenant_code") }}</b>
          </label>
          <label class="control-label text-left col-md-6">
            {{ $model->tenant_code }} 
          </label>
        </div>

        <div class="form-group col-md-6">
          <label class="control-label text-right col-md-6">
            <b>{{ $model->label("created_at") }}</b>
          </label>
          <label class="control-label text-left col-md-6">
            {{ date("d F Y", strtotime($model->created_at)) }} 
          </label>
        </div>

        <div class="form-group col-md-6">
          <label class="control-label text-right col-md-6">
            <b>{{ $model->label("ref_no") }}</b>
          </label>
          <label class="control-label text-left col-md-6">
            {{ $model->ref_no }} 
          </label>
        </div>

        <div class="form-group col-md-6">
          <label class="control-label text-right col-md-6">
            <b>{{ $model->label("amount") }}</b>
          </label>
          <label class="control-label text-left col-md-6">
            {{ currency($model->amount) }} 
          </label>
        </div>

        <div class="form-group col-md-6">
          <label class="control-label text-right col-md-6">
            <b>{{ $model->label("status") }}</b>
          </label>
          <label class="control-label text-left col-md-6">
            {!! $model->label("html.status.$model->status") !!}
          </label>
        </div>

        <div class="form-group col-md-6">
          <label class="control-label text-right col-md-6">
            <b>{{ $model->label("approved") }}</b>
          </label>
          <div class="col-md-6"> 
            @if ($model->status == App\Models\TopupRequest::STATUS_PENDING)
              {!! Form::text("approved", $model->amount) !!}
            @else
              {{ currency($model->approved) }}
            @endif
          </div>
        </div>

        @if (isset($model->img))
          <div class="form-group col-md-6">
            <label class="control-label text-right col-md-6">
              <b>{{ $model->label("img") }}</b>
            </label>
            <label class="control-label text-left col-md-6"> 
              <img src="{{ $model->img }}" class="thumbnail" />
            </label>
          </div>
        @endif
      </div>

      @if ($model->status == App\Models\TopupRequest::STATUS_PENDING)
        <div class="panel-heading">
          <div class="row text-center">
            <button type="submit" name="action" value="reject" class="btn btn-danger btn-lg">
              <i class="fa fa-remove"></i> Reject
            </button>
            
            <button type="submit" name="action" value="approve" class="btn btn-success btn-lg">
              <i class="fa fa-check"></i> Approve
            </button>
          </div>
        </div>
      @endif
    </div>

  {!! Form::close() !!}
@endsection
