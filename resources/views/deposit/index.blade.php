@extends("app")

@section("content")
  <table class="table table-bordered" datatable="{{ url("deposit/data") }}">
    <thead>
      <tr>
        <th dt-field="tenant_code"> {{ $model->label("tenant_code") }} </th>
        <th dt-field="created_at"> {{ $model->label("created_at") }} </th>
        <th dt-field="ref_no"> {{ $model->label("ref_no") }} </th>
        <th dt-field="amount"> Deposit </th>
        <th dt-field="status"> {{ $model->label("status") }} </th>
        <th dt-field="action" sort="false" search="false"> </th>
      </tr>
    </thead>
  </table>
@endsection