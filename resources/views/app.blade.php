@extends("layout.app")

@section("content-layout")
    {{-- Flash Messages --}}
    @if (Flash::exists())
        @foreach (Flash::pull() as $state => $messages)
            @foreach ($messages as $item)
                <div class="alert alert-{{ $state }}">
                    <button type="button" class="close" data-dismiss="alert">
                        <span>&times;</span>
                    </button>

                    {!! $item !!}
                </div>
            @endforeach
        @endforeach
    @endif

    {{-- Errors --}}
    @if (isset($errors) && count($errors) > 0)
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">
                <span>&times;</span>
            </button>
            
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- Page Body -->
    <div class="page-body"> @yield("content") </div>
@endsection