@extends("app")

@section("content")
  <table class="table table-bordered" datatable="{{ url("tenant/data") }}">
    <thead>
      <tr>
        <th dt-field="code"> {{ $model->label("code") }} </th>
        <th dt-field="name"> {{ $model->label("name") }} </th>
        <th dt-field="fullname"> {{ $model->label("fullname") }} </th>
        <th dt-field="user_license"> {{ $model->label("user_license") }} </th>
        <th dt-field="deposit"> {{ $model->label("deposit") }} </th>
        <th dt-col="#action" sort="false" search="false"> </th>
      </tr>
    </thead>

    <dt-template>
      <div id="action">
        <a href="{{ url("tenant/[[code]]") }}" class="btn btn-xs btn-info">
          <i class="fa fa-eye"></i> View Details
        </a>
      </div>
    </dt-template>
  </table>
@endsection
