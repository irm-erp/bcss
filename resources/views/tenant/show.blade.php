@extends("app")

@section("content")
  {!! Form::model($model, ["url" => "deposit/$model->id"]) !!}
    {{ method_field("PUT") }}

    <table class="table table-bordered">
      @if (isset($model->img))
        <tr>
          <td colspan="2"> 
            <img src="{{ $model->img }}" class="thumbnail" />
          </td>
        </tr>
      @endif
      <tr>
        <th class="text-right">{{ $model->label("tenant_code") }}</th>
        <td> {{ $model->tenant_code }} </td>
      </tr>
      <tr>
        <th class="text-right">{{ $model->label("req_at") }}</th>
        <td> {{ date("d F Y", strtotime($model->req_at)) }} </td>
      </tr>
      <tr>
        <th class="text-right">{{ $model->label("ref_no") }}</th>
        <td> {{ $model->ref_no }} </td>
      </tr>
      <tr>
        <th class="text-right">{{ $model->label("status") }}</th>
        <td>{{ $model->label("attr.status.$model->status") }}</td>
      </tr>
      <tr>
        <th class="text-right">{{ $model->label("amount") }}</th>
        @if ($model->status == App\Models\TopupRequest::STATUS_PENDING)
          <td> {!! Form::text("amount") !!} </td>
        @else
          <td> {{ currency($model->amount) }} </td>
        @endif
      </tr>

      @if ($model->status == App\Models\TopupRequest::STATUS_APPROVE)
        <tr>
          <th class="text-right">{{ $model->label("approved") }}</th>
          <td> {{ currency($model->approved) }} </td>
        </tr>
      @endif
    </table>

    @if ($model->status == App\Models\TopupRequest::STATUS_PENDING)
      <div class="row text-center">
        <button type="submit" name="action" value="reject" class="btn btn-danger btn-lg">
          <i class="fa fa-remove"></i> Reject
        </button>
        <button type="submit" name="action" value="approve" class="btn btn-success btn-lg">
          <i class="fa fa-check"></i> Approve
        </button>
      </div>
    @endif
  {!! Form::close() !!}
@endsection
