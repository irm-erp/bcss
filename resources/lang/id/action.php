<?php

return [
    // data list
	"view" => "Lihat",
	"update" => "Ubah",
    "delete" => "Hapus",
    "ready" => "Siap",
	"add" => "Tambah",

    // form
    "create" => "Buat Baru",
	"select" => "Pilih",
    "save" => "Simpan",
    "upload" => "Unggah",
    "back" => "Kembali",
    "cancel" => "Batal",

    // confirmation
    "yes" => "Ya",
    "no" => "Tidak",

    // account
    "logout" => "Keluar",
];