/**
 * Initialization JQuery Events
 * 
 * @param  mixed  scope  scope element to be render
 */
fn.event = {
    funcs: [],

    push: function(callback) {
        if (callback instanceof Function)
            this.funcs.push(callback);
    },

    trigger: function($$) {
        for (var i in this.funcs)
            this.funcs[i]($$);
    },

    init: function(scope) {
        var $$ = function(ele) {
            return $(ele, scope);
        };

        /** 
         * Input
         */
        // Format number
        // change value to formatted number on keyup
        $$("[num-format]").keyup(function(e) {
            var $this = $(this);
            var old_val = $this.val();
            var new_val = fn.format.num(old_val, $this.attr("num-format") || 0);
            
            if (old_val === new_val)
                return;

            var start = this.selectionStart;
            var end = this.selectionEnd;

            if (new_val.length > old_val.length) {
                start++; end++;
            }
            else if (new_val.length < old_val.length) {
                start--; end--;
            }

            $this.val(new_val);

            this.setSelectionRange(start, end);
        })
        .trigger("keyup")
        .parents('form').submit(function() {
            $$("[num-format]", this).each(function() {
                var $this = $(this);
                $this.val($this.val().replace(/(\.|\,0+)/g, ''));
            });
        })

        // check - uncheck all
        $$("[check-all]").change(function() {
            var $this = $(this);
            var target = $$($this.attr("check-all"));

            target.prop("checked", $this.prop("checked"))
        });

        // File input preview
        $$("input[type='file'][preview]").each(function() {
            var $this = $(this).clone();

            $("<div class='thumbnail mb-xs'>")
                .append($("<img>").attr("src", $this.attr("preview")))
                .insertBefore(this);
        });

        // Ajax data reference select2
        $("[ref-select]").each(function(v, i) {
            var sel = function(selector) {
                if (!!selector.match(/^[a-z]/i))
                    // find by name
                    return $("[name='"+ selector +"']");
                else 
                    // default jquery find
                    return $(selector);
            }

            var $this = $(this);
            var command = $this.attr("ref-select").split("|");
            var find = command[0].split(":");
            var url = $this.parents("[ref-url]").attr("ref-url") || "service/data";
            var target = sel(find[0]);

            target.on("change", function() {
                // parse ref-values format into object
                // e.g: name:value/selector/input-name, ..
                var values = $this.attr("ref-values");
                var data = {
                    command: command[1],
                    filter: find[1],
                    value: target.val(),
                };

                if (!fn.empty(values)) {
                    values = values.split(",");
                    var item, selector;

                    for (var i in values) {
                        item = values[i].split(":");
                        selector = sel(item[1]);

                        data[item[0]] = selector.length > 0 ? selector.val() : item[1];
                    }
                }

                $this.prop("disabled", true);

                $.ajax({
                    url: fn.url(url),
                    data: data,
                    type: "POST",
                    dataType: "json",
                    success: function(res) {
                        var default_options = $("[select2]:eq(2) option[value='']");
                        
                        $this.html(default_options.length == 0 ? "" : default_options);

                        var select2 = $this.select2({
                            data: res,
                            allowClear: true,
                            multiple: $this.is("[multiple]"),
                            placeholder: $this.is("[placeholder]") 
                                ? $this.attr("[placeholder]") : "Select..",
                            escapeMarkup: function(markup) {
                                return markup;
                            }
                        }).prop("disabled", false)

                        var value = $this.attr("value");

                        if (value)
                            select2.val(value).trigger("change");
                    },
                });
            })

            if (!fn.empty(target.val()))
                target.trigger("change");

            // remove attribute
            $this.removeAttr("ref-select");
        });

        /**
         * Validation
         */
        if (typeof Validator !== "undefined") {
            var v = {
                rules : {},

                validateInput: function(name, input) {
                    var data = {};
                    var rules = {};

                    data[name] = input.val();
                    rules[name] = this.rules[name];

                    // undefined rule would trigger error on validate
                    // catch error and ignore that shit
                    var validator;
                    var isValid = true;

                    try { 
                        validator = new Validator(data, rules);
                        isValid = validator.passes() 
                    } 
                    catch (e) {}

                    if (typeof isValid === "boolean") {
                        var group = input.closest('.form-group');

                        // input messages
                        var input_group = group.find(".input-group");

                        if (input_group.length > 0)
                            input = input_group;

                        var msg = input.parent().find(".help-block");

                        if (msg.length == 0) {
                            var msg_block = $("<p class='help-block'>");

                            input.parent().append(msg_block)
                        }

                        // success
                        if (isValid) {
                            group.removeClass('has-error').addClass('has-success');
                            msg.html("");
                        }
                        // fails
                        else {
                            group.removeClass('has-success').addClass('has-error');
                            msg.html(validator.errors.first(name));
                        }
                    }

                    return isValid;
                },
            };

            $$("form").each(function() {
                /*inputs*/
                var $this = $(this);
                var input = $this.find("[rules]");

                // initializing input and collecting rules
                input.each(function() {
                    var $this = $(this);
                    var name = $this.attr("name");
                    var rule = $this.attr("rules");

                    v.rules[name] = rule;

                    // adding star to required element
                    if (rule.indexOf("required") !== -1) {
                        var label = $this.closest(".form-group").find(".control-label")

                        if (!label.hasClass("label-marked")) {
                            label.append("<b class='text-danger'> *</b>")
                            label.addClass("label-marked")
                        }
                    }

                    var validate = function() {
                        return v.validateInput(name, $this);
                    };

                    // on change validation
                    $this.change(validate);
                    $this.keyup(validate);
                });

                /*Form*/
                // on submit validation
                $this.submit(function(e) {
                    var isValid = true;

                    input.each(function() {
                        var $this = $(this);

                        isValid = v.validateInput(this.name, $this);

                        // validation fails
                        if (!isValid) {
                            $this.focus();
                            e.preventDefault();
                        }

                        return isValid;
                    });
                })
            });
        }

        /** 
         * UI Element
         */
        // Datatables 
        if (!$.fn.DataTable.isDataTable("[datatable]")) {
            $$('[datatable]').each(function() {
                var datatable = $(this);
                var url = datatable.attr("datatable");

                // non-ajax datatable
                var options = {
                    bPaginate: datatable.attr("dt-paginate")!=="false",
                    initComplete: function(settings, json) {
                        // datatable.find("dt-template").remove();
                    },
                };

                if (fn.empty(url))
                    return $(this).DataTable(options);

                // ajax datatable
                var column = [];

                datatable.find("[dt-field],[dt-col]").each(function() {
                    var $this = $(this);
                    var name = $this.attr("dt-field");

                    fn.button = {};

                    if (!!$(this).attr('dt-col')) {
                        var content = datatable.parent()
                            .find("dt-template "+ $this.attr("dt-col"))
                            .html();

                        column.push({
                            bSearchable: $this.attr("search")!=="false",
                            bSortable: $this.attr("sort")!=="false",
                            mRender: function(data, type, row) {
                                if (fn.empty(content))
                                    return;

                                for (var i in row) {
                                    eval('var '+i+' = row[i];');
                                }

                                return content.replace(/\[\[(.+?)\]\]/g, function(match, syntax) {
                                    eval('var result = '+syntax);

                                    return fn.empty(result) ? "" : result;
                                });
                            }
                        })
                    }
                    else {
                        column.push({
                            data: name.substr(name.indexOf(".")+1),
                            name: name,
                            bSearchable: $this.attr("sort")!=="false",
                            bSortable: $this.attr("search")!=="false",
                        });
                    }
                })

                datatable.DataTable($.extend(options, {
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: column,
                }));

                datatable.on("draw.dt", function() {
                    fn.event.init(datatable);
                });
            });
        }

        // Modal
        $$("[modal],[modal-sm],[modal-md],[modal-lg]").click(function(e) {
            // must left button
            if (e.which != 1)
                return true;
            
            var button = $(this);
            var content = button.attr('modal-content') || '.page-content';
            var title = button.attr('modal-title') || $("title").text();
            var href = button.attr("href");

            // size
            var sizeSet = ["modal","modal-sm","modal-md","modal-lg"];

            for (var i in sizeSet) {
                // attribute is exists
                if (button.attr(sizeSet[i])!==undefined) {
                    var target = $(button.attr(sizeSet[i]) || '#modal-basic');

                    // set modal size
                    if (sizeSet[i]!='modal')
                        $(".modal-dialog", target).addClass(sizeSet[i]);
                    break;
                }
            }

            target.find(".modal-title").html(title);
            target.modal("show");

            if (href != undefined) {
                var body = target.find(".modal-body");
                var loading = $("#img-loading").clone();

                loading.removeAttr("id");
                loading.css({"margin-top": "100px"});
                body.html($("<div class='text-center' style='min-height:300px'>").append(loading));

                $.ajax({
                    url: href,
                    success: function(res) {
                        var content = $(res).find(content);

                        if (content.length == 0)
                            content = $(res);

                        // recompile angular if exists
                        /*if (isAngular) {
                            angular.element(document).injector().invoke(function($compile) {
                                var scope = angular.element(body).scope();
                                $compile(body)(scope);
                            })
                        }*/

                        body.html(content);
                        fn.event.init(content);
                    }
                })
            }

            e.preventDefault();
        });

        // Window Confirm
        $$("[confirm]").click(function(e) {
            if (e.which!=1)
                return true;

            var $this = $(this);
            var modal = fn.confirm({
                title: $this.attr("confirm-title"),
                body: $this.attr("confirm"),
                yes: function() {
                    $this[0].click();
                }
            });

            if (!modal.hasClass("in")) {
                e.stopImmediatePropagation();
                return false;
            }
        })

        $$("a[method]").click(function(e) {
            e.preventDefault();
            
            var $this = $(this);
            var form = $("<form method='POST' action='"+ $this.attr("href") +"' class='hide'>"+
                            +"<input name='_method' type='hidden' value='"+ $this.attr("method") +"'>"
                            +"<input name='_token' type='hidden' value='"+ fn.csrf +"'>"
                        +"</form>");

            $(document.body).append(form);
            
            form.submit();
        });

        // tooltip
        $$('[tooltip]').each(function() {
            var $this = $(this);
            var title = $this.attr("tooltip");

            if (!fn.empty(title))
                $this.attr("title", title);

            $this.tooltip();
        })

        this.trigger($$);
    },
}