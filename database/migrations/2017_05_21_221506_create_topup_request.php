<?php

use App\Models\TopupRequest;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopupRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("topup_request", function (Blueprint $table) {
            $table->increments("id");
            $table->string("ref_no", 50)->nullable();
            $table->decimal("amount", 15, 2);
            $table->decimal("approved", 15, 2)->nullable();
            $table->char("status", 1)->default(TopupRequest::STATUS_PENDING);
            $table->timestamps();

            $table->char("tenant_code", 8);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("topup_request");
    }
}
