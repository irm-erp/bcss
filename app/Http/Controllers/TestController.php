<?php

namespace App\Http\Controllers;

class TestController 
{
	public function index()
	{
		$http = new \GuzzleHttp\Client;

        try {
            $response = $http->post(env("BCSS_HOST")."/oauth/token", [
                "form_params" => [
                    "grant_type" => "password",
                    "client_id" => env("CLIENT_ID"),
                    "client_secret" => env("CLIENT_SECRET"),
                    "username" => "admin@app.com",
                    "password" => "admin",
                    "scope" => "*",
                ],
            ]);
            
            $data = json_decode((string) $response->getBody(), true);
            // $this->attributes["access_token"] = $data->access_token;
        }
        catch(\Exception $e) {
        	dd($e);
        	$content = $e->getResponse()->getBody()->getContents();
        	dd(json_decode($content));
        }

        dd($data);
	}
}
