<?php

namespace App\Http\Controllers;

use App\Models\TSS\Tenant;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use DB;
use Flash;

class TenantController 
{
    public function index()
    {
        return view("tenant.index", [
            "model" => new Tenant,
        ]);
    }

	public function data()
	{
        $res = Datatables::of(Tenant::query())->make(true);

        return response_map($res, function($item) {
            $item->join_date = date("d F Y", strtotime($item->join_date));
            $item->deposit = currency($item->claimable_deposit + $item->unclaimable_deposit);

            return $item;
        });
	}

    public function show($id)
    {
        return view("tenant.show", [
            "model" => Tenant::findOrFail($id),
        ]);
    }
}
