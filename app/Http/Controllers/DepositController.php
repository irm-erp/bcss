<?php

namespace App\Http\Controllers;

use App\Models\TSS\Tenant;
use App\Models\TopupRequest;
use DB;
use Flash;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class DepositController 
{
    public function index()
    {
        return view("deposit.index", [
            "model" => new TopupRequest,
        ]);
    }

	public function data()
	{
        $res = Datatables::of(TopupRequest::query())->make(true);

        return response_map($res, function($item) {
            $model = new TopupRequest;

            $item->amount = currency(isset($item->approved) ? $item->approved : $item->amount);
            $item->created_at = date("d F Y", strtotime($item->created_at));
            $item->action = 
                "<a href='".url("deposit", $item->id)."' class='btn btn-xs btn-info' modal-lg>
                    <i class='fa fa-eye'></i> View
                </a> ";

            $status_label = $model->label("attr.status.$item->status");

            switch ($item->status) {
                case TopupRequest::STATUS_PENDING:
                    $item->status = "<span class='label label-warning'>$status_label</span>";
                    break;

                case TopupRequest::STATUS_APPROVE:
                    $item->status = "<span class='label label-success'>$status_label</span>";
                    break;
                
                case TopupRequest::STATUS_REJECT:
                    $item->status = "<span class='label label-danger'>$status_label</span>";
                    break;
            }

            return $item;
        });
	}

    public function update(Request $req, $id)
    {
        try {
            DB::beginTransaction();
            $model = TopupRequest::findOrFail($id);

            switch ($req->action) {
                case "reject": $status = TopupRequest::STATUS_REJECT; break;
                case "approve": $status = TopupRequest::STATUS_APPROVE; break;
            }

            if ($model->status != TopupRequest::STATUS_PENDING) {
                throw new \Exception("Deposit sudah pernah di ".$model->label("attr.status.$model->status"));
            }

            $model->update([
                "status" => $status,
                "approved" => $req->approved,
            ]);

            Tenant::topup($model->tenant_code, $req->approved);
            Flash::success("Deposit berhasil di topup sebesar <b>".currency($req->approved)."</b>");

            DB::commit();
        }
        catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        return back();
    }

    public function show($id)
    {
        return view("deposit.show", [
            "model" => TopupRequest::findOrFail($id),
        ]);
    }
}
