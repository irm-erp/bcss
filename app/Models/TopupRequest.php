<?php 

namespace App\Models;

use DB;
use Illuminate\Http\UploadedFile;

class TopupRequest extends Model
{
    const STATUS_PENDING = "0";
    const STATUS_APPROVE = "1";
    const STATUS_REJECT = "2";

    protected $table = "topup_request";

    protected $fillable = ["ref_no", "amount", "approved", "tenant_code", "status", "img"];

    private $image;

    public function rules()
    {
        return [
            "tenant_code" => "required|string|max:8",
            "ref_no" => "required|string|max:50",
            "amount" => "required|numeric",
            "approved" => "numeric",
            "img" => "required|image",
        ];
    } 

    public function attributeLabels()
    {
        return [
            "tenant_code" => "Tenant",
            "ref_no" => "No Ref",
            "created_at" => "Request Date",
            "amount" => "Deposit Requested",
            "updated_at" => "Approved Date",
            "approved" => "Deposit Approved",
            "img" => "Image",
            "attr" => [
                "status" => [
                    self::STATUS_PENDING => "Pending",
                    self::STATUS_APPROVE => "Approved",
                    self::STATUS_REJECT => "Rejected",
                ],
            ],
            "html" => [
                "status" => [
                    self::STATUS_PENDING => "<span class='label label-warning'>Pending</span>",
                    self::STATUS_APPROVE => "<span class='label label-success'>Approved</span>",
                    self::STATUS_REJECT => "<span class='label label-danger'>Rejected</span>",
                ],
            ],
        ];
    }

    public function getImgAttribute()
    {
        $file = "topup/".$this->getKey().".jpg";

        return config("app.tss.url")."/image?path=".rawurlencode($file);
    }
}