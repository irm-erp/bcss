<?php 

namespace App\Models\Auth;

use App\Models\Model;

// use Illuminate\Foundation\Auth\User as Authenticatable;

class Authenticatable /*extends Model*/
{
    use \Zizaco\Entrust\Traits\EntrustUserTrait;
}