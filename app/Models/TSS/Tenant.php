<?php

namespace App\Models\TSS;

use App\Models\Model;
use DB;

class Tenant extends Model
{
    protected $connection = "tss";

    protected $table = "tenant";

    public $primaryKey = "code";

    public $timestamps = false;

    public function attributeLabels()
    {
        return [
            "code" => "Code",
            "name" => "Name",
            "join_date" => "Join Date",
            "fullname" => "Fullname",
            "address" => "Address",
            "city" => "City",
            "tech_contact_name" => "Technical Contact Name",
            "tech_contact_phone" => "Technical Contact Phone",
            "tech_contact_mail" => "Technical Contact Mail",
            "comm_contact_name" => "Commercial Contact Name",
            "comm_contact_phone" => "Commercial Contact Phone",
            "comm_contact_mail" => "Commercial Contact Mail",
            "claimable_deposit" => "Claimable Deposit",
            "unclaimable_deposit" => "Unclaimable Deposit",
            "user_license" => "User License",
        ];
    }

    public function scopeTopup($query, $tenant_code, $amount)
    {
        if (!is_numeric($amount)) {
            throw \Exception("Topup harus benilai angka");
        }

        return $query
            ->where("code", $tenant_code)
            ->update(["claimable_deposit" => DB::raw("claimable_deposit + $amount")]);
    }
}