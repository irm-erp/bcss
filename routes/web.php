<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::auth();

Route::group(["middleware" => "auth"], function() {
	Route::get('/', function () {
	    return view('app');
	});

	Route::any("deposit/data", "DepositController@data");
	Route::resource("deposit", "DepositController", ["only" => ["index", "update", "show"]]);

	Route::any("tenant/data", "TenantController@data");
	Route::resource("tenant", "TenantController", ["only" => ["index", "show"]]);
});

Menu::make("main", function($menu) {
	$menu->add("Home", "/", "fa fa-home");
	$menu->add("Tenant", "tenant", "fa fa-users");
	$menu->add("Topup Request", "deposit", "fa fa-money");
});
